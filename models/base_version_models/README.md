# Models source

At tinkercad:

* https://www.tinkercad.com/things/hQ6VmVerEWm
* https://www.tinkercad.com/things/ehEvOBV0VGe

# Changelog

## v5

* Base models from version `0.4.0` and variant `B4`.

Available at: https://www.tinkercad.com/things/hQ6VmVerEWm-dremel-cnc-2/

Note: the `B4` variant of the Z-axis structure is compatible with the "pipettin-grbl" proyect's mount.

## v4

Previous development models available at: https://www.tinkercad.com/things/hQ6VmVerEWm-dremel-cnc-2/edit
