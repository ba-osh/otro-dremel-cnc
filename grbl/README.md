# Modifications to GRBL 1.1h

Son dos modificaciones.

## Disable variable spindle

En `config.h` comentar la línea: `#define VARIABLE_SPINDLE`. 

## Enable double Y axis

Habilitar el doble eje Y (eje Y con dos motores con homing independiente).

En `config.h`, editar algunas líneas para que queden como se indica:

* Línea 641 (aprox): `#define ENABLE_DUAL_AXIS`
* Línea 644 (aprox): `#define DUAL_AXIS_SELECT  Y_AXIS`
* Línea 676 (aprox): `#define DUAL_AXIS_CONFIG_CNC_SHIELD_CLONE`

## Fix a bug

Es posible que el código tenga un typo, y la compilación falle para el doble eje Y.

El typo está en `report.c`, ver: https://github.com/gnea/grbl/commit/bdc2da9b5972c5972cdd4a4f045a575f09ff74cc

Hay que agregar un `}` al final de la línea 581 (aprox, ver commit en el enlace anterior).
