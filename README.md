
> The models in this project have been superseeded another project: https://gitlab.com/naikymen/custom_cnc

> Los modelos en este proyecto fueron superados en otro proyecto: https://gitlab.com/naikymen/custom_cnc


# Otro Dremel CNC

Modelos y documentación para armar un "router CNC" de bajo costo y de fuente abierta. Basado en Dremel, partes impresas en 3D y otras partes genéricas.

La génesis de este proyecto fue el "miniCNC" de la residencia de hardware libre "[reGOSH 2019](http://regosh.libres.cc/)"" que hicimos en el laboratorio [CTA](https://cta.if.ufrgs.br/capa/) de la Universidade Federal do Rio Grande do Sul.

Ahí aprendimos las bases del CNC y del trabajo en hardware libre, a partir de electrónica reciclada y software libre. El proyecto y su documentación están disponibles en su GitLab: https://tecnologias.libres.cc/explore/projects

Aquí adoptamos el concepto de "Hiperobjeto" e intentamos aplicarlo a este proyecto de un CNC no-tan-mini :)

# Yet another Dremel CNC

* Mostly 3D-printed.
* Non 3D-printed parts include rods, motors, bearings, the Dremel, etc.

We did our best to get a decent CNC for simple milling proyects.

Read about our CNC adventure here: https://wiki.frubox.org/proyectos/diy/cnc

We hope you'll find the documentation there sufficient not only to build an open-hardware CNC, but also to use it entirely within an open-source software pipeline.

# Use cases

## PCBs

It works!

https://wiki.frubox.org/proyectos/diy/cnc/kicad_pcb

![alt text](doc/pcb.png)

## Acrylic

It works!

https://wiki.frubox.org/proyectos/diy/cnc/acrilico

![alt text](doc/acrilico.png)
