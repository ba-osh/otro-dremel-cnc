
Import the SVG file into the online CAM software from OpenBuilds.

* https://cam.openbuilds.com/
* https://github.com/OpenBuilds/OpenBuilds-CAM

There is a "bug" that confuses "feedrate" with "plungerate".

* For a patch see: https://github.com/OpenBuilds/OpenBuilds-CAM/issues/58
